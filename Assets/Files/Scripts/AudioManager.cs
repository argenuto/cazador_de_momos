﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[System.Serializable]
public class WithStringEvent : UnityEvent<string,string>
{
}
public enum stage {mainmenu,trail};
public class AudioManager : MonoBehaviour {
	public stage stg;
	public static AudioManager aumanger;
	// Use this for initialization
	void OnValidate (){
		aumanger = this;
	}
	void Start () {
		OnValidate ();
		Debug.Log (StateGame.g_state.ambiencesound);
		FindAndPlayClip (StateGame.g_state.ambiencesound);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PlayMainThemeByChara (string chrctr,string le_clip)
	{
		bool is_chara = false;
		string clp;
		StateGame sg = StateGame.g_state;
		string[] chara = { sg.runner_body, sg.runner_feet, sg.runner_head, sg.runner_legs };
		int i = 0;
		do{
			Debug.Log(chara.Length+" : "+i);
			is_chara = ( chara[i] == chrctr? true:false);
			i++;

		}while(is_chara == true && i < chara.Length) ;
		clp = (is_chara ? le_clip : "pumpedupkids");
		StateGame.g_state.SaveAmbienceSound (clp);
		FindAndPlayClip (clp);
	} 


	void FindAndPlayClip(string le_clip){
		
		AudioClip aclip = Resources.Load<AudioClip> ("sounds/"+ le_clip+stg.ToString());
		this.GetComponent<AudioSource> ().clip = aclip;
		this.GetComponent<AudioSource> ().Play();
	}


}

