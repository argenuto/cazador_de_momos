﻿using UnityEngine;
using System.Collections;

public class momo_creator : MonoBehaviour {
	public bool ismoving = false;
	public float init_time = 1f;
	public float end_time = 40f;
	GameObject[] momos;

	// Use this for initialization
	void Start () {
		momos = Resources.LoadAll<GameObject>("prefabs/momos");
				/*if (ismoving) {
			GenerateMovingMomos ();
		} else {
			GenerateMomos ();
		}*/


	
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnEnable(){
		if (ismoving) {
			momos = Resources.LoadAll<GameObject>("prefabs/momos");
			StartCoroutine (CorrutineGenerateMovingMomos());
		} else {
			StartCoroutine (CorrutineGenerateMomos());
		}
	}

	void OnDisable(){
		StopAllCoroutines ();
	}
	/*void GenerateMomos()
	{
		Instantiate (momos[Random.Range (0, momos.Length)], new Vector3 (transform.position.x, transform.position.y, 0), Quaternion.identity);
		Invoke("GenerateMomos",Random.Range(init_time,end_time));
		Debug.Log ("re100 horneado");
	}
	void GenerateMovingMomos () {
		GameObject mov_mom = (GameObject) Instantiate (momos[Random.Range (0, momos.Length)], new Vector3 (transform.position.x, transform.position.y, 0), Quaternion.identity);
		mov_mom.GetComponent<Rigidbody2D> ().velocity = Vector2.left * 3;
		Invoke("GenerateMovingMomos",Random.Range(init_time,end_time));
	}*/

	IEnumerator CorrutineGenerateMomos(){
		yield return new WaitForSeconds (Random.Range (init_time, end_time));
		while (true) {
			Instantiate (momos [Random.Range (0, momos.Length)], new Vector3 (transform.position.x, transform.position.y, 0), Quaternion.identity);
			yield return new WaitForSeconds (Random.Range (init_time, end_time));
		}
	}

	IEnumerator CorrutineGenerateMovingMomos(){
		while (true) {
			GameObject mov_mom = (GameObject) Instantiate (momos[Random.Range (0, momos.Length)], new Vector3 (transform.position.x, transform.position.y, 0), Quaternion.identity);
			mov_mom.GetComponent<Rigidbody2D> ().velocity = Vector2.left * 5;
			yield return new WaitForSeconds(Random.Range(init_time,end_time));
		}
	}
}
