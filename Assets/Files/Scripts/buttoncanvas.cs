﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
public class buttoncanvas : MonoBehaviour {
	public Button button;
	public int scene;
	// Use this for initialization
	void Start () {
		button = GetComponent<Button>();
		button.onClick.AddListener (LoadScene);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void LoadScene(){
		SceneManager.LoadScene (scene);
	}
}
