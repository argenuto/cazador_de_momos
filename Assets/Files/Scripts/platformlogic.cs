﻿using UnityEngine;
using System.Collections;

public class platformlogic : MonoBehaviour {
	public int points = 0;

	private bool colidedwithdordo = false;
	Animator anim;
	public bool combo = false;
	public bool its_a_trap = false;
	[Range(0,1)]
	public float frequency = 2;
	[Range(0,1)]
	public float lvl_1 = 0;
	[Range(0,1)]
	public float lvl_4 =1;
	int[] h;
	void OnValidate(){
		frequency = lvl_1;
//		Debug.Log ("si ves esto 2 veces en " + this.gameObject.name + ", la cosa esta bien..");

	}
	// Use this for initialization
	void OnEnable(){
	}
	void Start () {
		if (!its_a_trap) {
			anim = GetComponent<Animator> ();
		}
		NotificationCenter.DefaultCenter ().AddObserver (this, "PlatComboStarted");
		NotificationCenter.DefaultCenter ().AddObserver (this, "PlatComboRuined");

	}
	
	// Update is called once per frame
	void Update () {
		if (!its_a_trap) {
			anim.SetBool ("iscombo", combo);
		}
	}
	void OnCollisionEnter2D(Collision2D collision){
		if (!colidedwithdordo && collision.gameObject.tag == "Player") {
			colidedwithdordo = true;
			GameObject obj = collision.contacts [0].collider.gameObject;
			if (obj.name == "zapatofrontal" || obj.name == "zapatotrasero") {
				colidedwithdordo = true;
				NotificationCenter.DefaultCenter ().PostNotification (this, "ScoreIncrease", points);
				NotificationCenter.DefaultCenter ().PostNotification (this, "PlatformSteped", gameObject);
				GameObject.Find ("Camera").GetComponent<Score> ().IncreasePlatCounter ();
//				Debug.Log ("llamado papu!");
			}
		}


		}

	void PlatComboStarted(Notification notification){
		if (this.gameObject.tag == (string)notification.data) {
			combo = true;
		} 
	}

	void PlatComboRuined (Notification notification)
	{
		combo = false;

	}
	public void ModifyFrequency(float modfactor){
		frequency = lvl_1*Mathf.Cos (modfactor * Mathf.PI) + lvl_4* Mathf.Sin (modfactor * Mathf.PI);
	}
}


	
