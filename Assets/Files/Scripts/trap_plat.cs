﻿using UnityEngine;
using System.Collections;
public enum condition{isbomb,istrap};
public class trap_plat : MonoBehaviour {
	public condition cndtn;
	public Vector2 expultion_force;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnCollisionEnter2D(Collision2D collision){
		if (collision.gameObject.tag == "Player") {
			switch (cndtn) {
			case condition.isbomb:
				GameObject.Find ("Player").GetComponent<Rigidbody2D> ().AddForce (expultion_force);
				Destroy (gameObject);
				break;
			case condition.istrap:
				Destroy (gameObject);
				break;
			}
		}
	}
}
