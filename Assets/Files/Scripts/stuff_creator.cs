﻿using UnityEngine;
using System.Collections;

public class stuff_creator : MonoBehaviour {
	/*public float init_time = 1f;
	public float end_time = 2f;*/
	public float min_dis = 1f;
	public float max_dis = 2f;
	bool horizontal, vertical;
	Vector3 direction;
	public GameObject[] platforms;
	public int[] plat_chance_table,aliast;
	public float[] prob_table;

	public bool right_stuff_activator = false;
	public bool left_stuff_activator = false;
	int platform_separator_value = 0;


	void OnValidate(){
		platforms = Resources.LoadAll<GameObject>("prefabs/platforms"); /*GameObject.FindObjectOfType<stuff_container> ().platform_container;*/
		foreach (GameObject p in platforms) {
			p.GetComponent<platformlogic> ().frequency = p.GetComponent<platformlogic> ().lvl_1;
		}
		CreateAliasTables ();

		/*plat_chance_table = CreateChanceTable (platforms, TotalWeight (platforms));
		for(int i = 0;i<plat_chance_table.Length;i++) {
			Debug.Log ("en la casilla numero "+i+" esta: "+plat_chance_table[i]);
		}*/


	}
	void Awake(){
		OnValidate ();
		if (platforms [0].GetComponent<PlatformEffector2D> ().useOneWay == false) {
			SwitchPlats ();
		}
			
	}
	// Use this for initialization
	void Start () {
		platforms = Resources.LoadAll<GameObject>("prefabs/platforms"); /*GameObject.FindObjectOfType<stuff_container> ().platform_container;*/
		//GeneratePlatforms ();
		/*int i = 0;
		for(i = 1, i <=5,i =+ 1);*/
		/*NotificationCenter.DefaultCenter ().AddObserver (this, "GoDown");
		NotificationCenter.DefaultCenter ().AddObserver (this, "GoUp");
		NotificationCenter.DefaultCenter ().AddObserver (this, "GoRight");
		NotificationCenter.DefaultCenter ().AddObserver (this, "GoLeft");*/

	}
	public void GoDown(){
		StopAllCoroutines ();
		Debug.Log("pa abajo!");
		horizontal = false;
		vertical = true;
		direction = new Vector3(0f,-1f,0f);
//		Debug.Log ("magnitud: " + direction);
//
		Debug.Log(direction);
		StartCoroutine(CorroutineGenObsDistance());
	}

	public void GoUp(){
//		Debug.Log("pa arriba!");
		horizontal = false;
		vertical = true;
		direction = new Vector3 (0,1,0);
		StartCoroutine(CorroutineGenObsDistance());
//		Debug.Log ("direccion del notificador: " +direction);
	}

	public void GoRight(){
//		Debug.Log("pa alla!");
		horizontal = true;
		vertical = false;
		direction = new Vector3 (1,0,0);
		StartCoroutine(CorroutineGenObsDistance());
		//Debug.Log ("direccion del notificador: " +direction);
	}

	public void GoLeft(){
//		Debug.Log("pa aca!");
		horizontal = true;
		vertical = false;
		direction = new Vector3 (-1,0,0);
		StartCoroutine(CorroutineGenObsDistance());
		//Debug.Log ("direccion del notificador: " +direction);
	}

	// Update is called once per frame
	void Update () {

	}

	void OnEnable(){
		//StartCoroutine (CorrutineGeneratePlatforms ());

		//StartCoroutine(CorroutineGenObsDistance());

	}
	void OnDisable(){

		StopAllCoroutines ();
	}
	/*void GeneratePlatforms()
	{
		Instantiate (platforms[Random.Range (0, platforms.Length)] , new Vector3 (transform.position.x,transform.position.y,0), Quaternion.identity);
		Invoke("GeneratePlatforms",Random.Range(init_time,end_time));
	}*/

	/*IEnumerator CorrutineGeneratePlatforms(){
		while (true) {
			Instantiate (platforms [Random.Range (0, platforms.Length)], new Vector3 (transform.position.x, transform.position.y, 0), Quaternion.identity);
			yield return new WaitForSeconds (Random.Range (init_time, end_time));
		}
	}*/


	IEnumerator CorroutineGenObsDistance ()
	{
	yield return new WaitForEndOfFrame();
		
//	Debug.Log ("empeze ya con la direction a :"+direction);

	GameObject stuff = platforms [TakePlatform()];
		stuff.tag = this.tag;

		Instantiate (stuff, new Vector3 (transform.position.x, transform.position.y, 0), Quaternion.identity);
		
	/*if (!vertical) {
			yield return new WaitForSeconds (0.5f);
		}*/
		Vector3 nxtpnt = transform.position + (direction * Random.Range (min_dis, max_dis));
		while (true) {
		stuff = platforms [TakePlatform()];
			stuff.tag = this.tag;

			Vector3 actlpnt = transform.position;
			if ((Mathf.Ceil(actlpnt.x) == Mathf.Ceil(nxtpnt.x)) && horizontal) {
				Instantiate (stuff, new Vector3 (transform.position.x, transform.position.y, 0), Quaternion.identity);
				nxtpnt = transform.position + (direction * Random.Range (min_dis, max_dis));
			}
			if ((Mathf.Ceil(actlpnt.y) == Mathf.Ceil(nxtpnt.y)) && vertical) {
				if (platform_separator_value != 0) {
					Debug.Log ("viene con ñapa");
			}
			Instantiate (stuff, new Vector3 (transform.position.x, transform.position.y, 0), Quaternion.identity);
			nxtpnt = transform.position + (direction * (Random.Range (min_dis, max_dis)+platform_separator_value));
			}
			if (tag == "visto") {
//				Debug.Log ("punto actual: " + actlpnt);
//			Debug.Log ("proximo punto: " + nxtpnt+" que es asi la distancia actual mas"+(direction * Random.Range (min_dis, max_dis))+"unidades");
//				Debug.Log ("distancia: " + (nxtpnt - actlpnt).magnitude);
			}

		yield return new WaitForFixedUpdate();
		}
	}

float TotalWeight(GameObject[] stuff_list){
		float weight = 0;
		foreach (GameObject go in stuff_list) {
			weight += go.GetComponent<platformlogic> ().frequency;
			
			
		}
		return weight;
	}


/*int[] CreateChanceTable(GameObject[] stuff_list,int ttlwght){
	int[]	chance_table = new int[ttlwght];
	int ct_indx = 0;
	for(int i = 0;i<stuff_list.Length;i++) {
			//Debug.Log ("estoy en " + i);
		float freq = stuff_list[i].GetComponent<platformlogic> ().frequency;
			//Debug.Log ("mi frequencia es de : "+freq);
			while (freq > 0) {
				chance_table [ct_indx] = i;
				freq--;
				ct_indx++;
		}
	}
	return chance_table;

}*/
 
public void CreateAliasTables(){
	//create variables
	float[] norm_tbl = new float[platforms.Length];
	int items = platforms.Length;
	prob_table = new float[items];
		aliast = new int[items];
//Create 2 WorkList: Small & Large
	ArrayList large = new ArrayList(),small = new ArrayList();

	float totalweight = TotalWeight (platforms);
	Debug.Log ("peso total: "+totalweight);
	//set probability table
		if (totalweight > 0) {
			for (int i = 0; i < items; i++) {
			float freq = platforms [i].GetComponent<platformlogic> ().frequency;
//			Debug.Log ("frequency: " + freq);
				float prob = (freq/totalweight) * items;
//Debug.Log ("probability at item #" + i + ": " + prob);
				norm_tbl [i] = prob;
				// set the list with index numbers of the stuff list according to their sizes
				if (prob >= 1) {
					large.Add (i);
				} else {
					small.Add (i);
				}
			} 
			
		} else {
			Debug.Log ("prro, no hay nada... :'v");
		}
	
//		Debug.Log ("largesize: " + large.Count);
//	Debug.Log ("smallsize: " + small.Count);

		while (large.Count > 0 && small.Count > 0) {

		int l = (int) large [0], g = (int) small[0];
			large.RemoveAt (0);
			small.RemoveAt (0);
		prob_table[g] = norm_tbl[g];
			aliast[g] = l;
			norm_tbl [l] = (norm_tbl [l] + norm_tbl [g]) - 1;

		if (norm_tbl [l] >= 1) {
				large.Add (l);
			//	Debug.Log ("bien");
			} else {
				small.Add (l);
			//	Debug.Log ("Bien x2");
		}

	}
	//Debug.Log ("largesize after elimination: " + large.Count);
	//Debug.Log ("smallsize after elimination: " + small.Count);
	/*foreach (object o in large) {
		Debug.Log ((int)o+"con probabilidad: "+norm_tbl[(int)o]);

	}*/
	while(large.Count > 0) {
		int l = (int)large [0];
			large.RemoveAt (0);
			norm_tbl [l] = 1;
		prob_table[l] = norm_tbl[l];
	}


	while(small.Count > 0) {
		int g = (int) small [0];
		small.RemoveAt (0);
		norm_tbl [g] = 1;
		prob_table[g]= norm_tbl[g];
	
	}


	}
public int TakePlatform(){
		int plat_num = 0;	
	int dice = Random.Range (0, prob_table.Length);
		float coin = Random.Range (0, 1f);
		if (coin <= prob_table [dice]) {
			plat_num = dice;
		} else {
			plat_num = aliast [dice];
	}
	return plat_num;
}

void FixedUpdate(){
		if (right_stuff_activator) {
		RaycastHit2D plt = Physics2D.Raycast (transform.position, Vector2.right,Mathf.Infinity,LayerMask.GetMask("ground"));
		if (plt.collider != null) {
			//	Debug.Log ("piu piu diestro!");
			//Debug.Log ("esta en el layer #"+plt.transform.gameObject.layer);
			Debug.DrawLine (transform.position, plt.transform.position);
			platform_separator_value = 4*(int)direction.y;
		}

	}
		if (left_stuff_activator) {
		RaycastHit2D plt = Physics2D.Raycast (transform.position, Vector2.left,Mathf.Infinity,LayerMask.GetMask("ground"));
			if (plt.collider != null) {
			//Debug.Log ("piu piu siniestro!");
			Debug.DrawLine (transform.position, plt.transform.position);
			platform_separator_value = 4*(int)direction.y;
			} else {
				platform_separator_value = 0;
		}


		}
	}
public void ChangeFrequency(float factor){
		foreach (GameObject go in platforms) {
			go.GetComponent<platformlogic> ().ModifyFrequency (factor);
	}
}
public void SwitchPlats(){
		foreach (GameObject go in platforms) {
		go.GetComponent<PlatformEffector2D> ().useOneWay = !go.GetComponent<PlatformEffector2D> ().useOneWay;
			Debug.Log (go.GetComponent<PlatformEffector2D> ().useOneWay);
	}
}
}

	


	

