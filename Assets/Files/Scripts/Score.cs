﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {
	public int score = 0;
	public int plat_count = 0;
	public int momo_count = 0;
	public Text marker;
	// Use this for initialization
	void Start () {
		NotificationCenter.DefaultCenter ().AddObserver (this, "ScoreIncrease");
		NotificationCenter.DefaultCenter ().AddObserver (this, "DordoIsdead");
		UpdateMarker ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void ScoreIncrease(Notification notification){
		int increasingscore = (int)notification.data;
		score += increasingscore;
		UpdateMarker ();
		//Debug.Log ("llevas " + score + " puntazos alv >:v");
	//	Debug.Log(96/StateGame.g_state.difficulty);

	}
	void UpdateMarker(){
	marker.text = score.ToString ("D5");
	}
	void DordoIsdead(Notification notification){
		if (score > StateGame.g_state.hi_score) {
			StateGame.g_state.hi_score = score;
			StateGame.g_state.SaveScore (score,StateGame.g_state.momos,StateGame.g_state.plats);
//			Debug.Log ("re100 guardado");
		}
		if (plat_count > StateGame.g_state.plats) {
			StateGame.g_state.plats = plat_count;
			StateGame.g_state.SaveScore (StateGame.g_state.hi_score,StateGame.g_state.momos,plat_count);
		}
		if (momo_count > StateGame.g_state.momos) {
			StateGame.g_state.momos = momo_count;
			StateGame.g_state.SaveScore (StateGame.g_state.hi_score,momo_count,StateGame.g_state.plats);
		}
	}

	public void IncreasePlatCounter(){
		plat_count++;
//		Debug.Log (plat_count);
	}
	public void IncreaseMomocounter(){
		momo_count++;
//		Debug.Log (momo_count);
	}

}
