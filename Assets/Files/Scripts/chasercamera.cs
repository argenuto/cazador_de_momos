﻿using UnityEngine;
using System.Collections;

public class chasercamera : MonoBehaviour {
	public Transform trns;
	public float diferencia = 5;
	public float speedx = 8, speedy = 8,speed_factor = 0.0003f;
	public bool free_roaming, moving_left, moving_right, moving_up, moving_down,random;
	public GameObject r_destr, l_destr,l_edge_destr, a_destr,a_edge_destr, b_destr,b_edge_destr,l_gens,r_gens,b_gens,a_gens;
	public control_dordo le_cdordo;

	// Use this for initialization

	void Start () {
		if (random) {
			int rndm_nmbr = UnityEngine.Random.Range (0, 4);
			switch (rndm_nmbr) {
			case 0:
				GoUp ();
				break;
			case 1:
				GoDown ();
				break;
			case 2:
				GoRight ();
				break;
			case 3:
				GoLeft ();
				break;
			}

		}
		/*NotificationCenter.DefaultCenter ().AddObserver (this, "GoDown");
		NotificationCenter.DefaultCenter ().AddObserver (this, "GoUp");
		NotificationCenter.DefaultCenter ().AddObserver (this, "GoRight");
		NotificationCenter.DefaultCenter ().AddObserver (this, "GoLeft");
		StopCoroutine ("moving");*/
		Resources.UnloadUnusedAssets ();

	}
	void Awake(){
		
		GeneratorSwitcher(a_gens,false);
		GeneratorSwitcher(b_gens,false);
		GeneratorSwitcher(r_gens,false);
		GeneratorSwitcher(l_gens,false);
		GeneratorSwitcher(b_gens,false);

	}
	void OnEnable(){
		StartCoroutine(Accel(StateGame.g_state.difficulty));

	}
	void FixedUpdate(){
	}
	// Update is called once per frame
	void Update () {

		if (free_roaming) {
			transform.position = new Vector3 (trns.position.x + diferencia, transform.position.y, transform.position.z);
		}
		if (moving_right) {
			/*GeneratorSwitcher (a_gens, false);
			GeneratorSwitcher (b_gens, false);
			GeneratorSwitcher (r_gens, true);
			GeneratorSwitcher (l_gens, false);
			GeneratorSwitcher (b_gens, false);
			r_destr.SetActive (false); 
			l_destr.SetActive (true); 
			a_destr.SetActive (true); 
			b_destr.SetActive (true);*/
			StartCoroutine (moving (speedx, 0, speed_factor));

		}
		if (moving_left) {
			/*GeneratorSwitcher (a_gens, false);
			GeneratorSwitcher (b_gens, false);
			GeneratorSwitcher (r_gens, false);
			GeneratorSwitcher (l_gens, true);
			GeneratorSwitcher (b_gens, false);
			r_destr.SetActive (true); 
			l_destr.SetActive (false); 
			a_destr.SetActive (true); 
			b_destr.SetActive (true);*/
			StartCoroutine (moving (-1 * speedx, 0, speed_factor));
		}
		if (moving_down) {
			/*GeneratorSwitcher (a_gens, false);
			GeneratorSwitcher (b_gens, true);
			GeneratorSwitcher (r_gens, false);
			GeneratorSwitcher (l_gens, false);
			r_destr.SetActive (false); 
			l_destr.SetActive (false); 
			a_destr.SetActive (true); 
			b_destr.SetActive (true);
			b_edge_destr.SetActive (true);
			a_edge_destr.SetActive (true);*/

			StartCoroutine (moving (0, -1 * speedy, speed_factor));
		}
		if (moving_up) {
			/*	GeneratorSwitcher (a_gens, true);
			GeneratorSwitcher (b_gens, false);
			GeneratorSwitcher (r_gens, false);
			GeneratorSwitcher (l_gens, false);
			GeneratorSwitcher (b_gens, false);
			r_destr.SetActive (false); 
			l_destr.SetActive (false); 
			a_destr.SetActive (false); 
			b_destr.SetActive (true);
			b_edge_destr.SetActive (true);
			a_edge_destr.SetActive (false);*/
			StartCoroutine (moving (0, speedy, speed_factor));
		}
		/*	if (moving_right) {
			NotificationCenter.DefaultCenter ().PostNotification (this, "GoRight");
		}
		if (moving_up) {
			NotificationCenter.DefaultCenter ().PostNotification (this, "GoUp");
		}
		if (moving_left) {
			NotificationCenter.DefaultCenter ().PostNotification (this, "GoLeft");
			//Debug.Log ("arranqué");
		}
		if (moving_down) {
			NotificationCenter.DefaultCenter ().PostNotification (this, "GoDown");
		}*/


	}


	IEnumerator moving(float x_axe,float y_axe,float speed_factor){

		transform.Translate((new Vector2 (x_axe,y_axe)) * Time.deltaTime);
		speedx +=  speed_factor;
		speedy +=  speed_factor;
		le_cdordo.velocity +=(speed_factor);
		yield return null;


	}

	IEnumerator Accel(int difficulty){
		float factor = 0;
		float init_sfctr = speed_factor;
		//int j = 1;
		GameObject[] gens = {a_gens,b_gens,l_gens,r_gens};
		Debug.Log ("esperandote...");

		yield return new WaitForSeconds (Random.Range(5f,10f)-difficulty);;
		//Debug.Log ("ia yege :v");

		while (factor <= 1) {
			foreach (GameObject g in gens) {
				foreach(Transform gns in g.transform){
					if (gns.GetComponent<stuff_creator> () != null) {
						gns.GetComponent<stuff_creator> ().ChangeFrequency (factor);
						gns.GetComponent<stuff_creator> ().CreateAliasTables ();
						if (factor >= 0.5f / difficulty &&
							gns.GetComponent<stuff_creator>()
								.platforms [0].GetComponent<PlatformEffector2D> ()
									.useOneWay == true) {
							gns.GetComponent<stuff_creator> ().SwitchPlats ();
							//Debug.Log ("cerradisimos");
						}
					}


				}


			}

			factor += 0.024f * difficulty;
			speed_factor += (init_sfctr / 4) * difficulty;
			Debug.Log ("esperandito...");

			//int k = 96 / difficulty;

			yield return new WaitForSeconds (Random.Range(7f,12f)-difficulty);;
		//	j++;
		//	Debug.Log ("La Espera Acabo...");
			Debug.Log ("speedfactor: " + speed_factor);
			Debug.Log ("factor: " + factor);
		}
		yield return null;
	}

	void GoDown(){
		StopCoroutine ("moving");
		moving_down = true;
//		Debug.Log ("saludos...");
		moving_up = false;
		moving_left = false;
		moving_right = false;
		GeneratorSwitcher (a_gens, false);
		GeneratorSwitcher (b_gens, true);
		GeneratorSwitcher (r_gens, false);
		GeneratorSwitcher (l_gens, false);
		r_destr.SetActive (false); 
		l_destr.SetActive (false); 
		a_destr.SetActive (true); 
		b_destr.SetActive (true);
		b_edge_destr.SetActive (true);
		a_edge_destr.SetActive (true);
		NotificationCenter.DefaultCenter ().PostNotification (this,"GoDown");

	}

	void GoUp(){
		StopCoroutine ("moving");
//		Debug.Log ("saludos...");
		moving_down = false;
		moving_up = true;
		moving_left = false;
		moving_right = false;
		GeneratorSwitcher (a_gens, true);
		GeneratorSwitcher (b_gens, false);
		GeneratorSwitcher (r_gens, false);
		GeneratorSwitcher (l_gens, false);
		GeneratorSwitcher (b_gens, false);
		r_destr.SetActive (false); 
		l_destr.SetActive (false); 
		a_destr.SetActive (false); 
		b_destr.SetActive (true);
		b_edge_destr.SetActive (true);
		a_edge_destr.SetActive (false);
		NotificationCenter.DefaultCenter ().PostNotification (this,"GoUp");

	}
	void GoRight(){
		StopCoroutine ("moving");
		moving_down = false;
		moving_up = false;
		moving_left = false;
		moving_right = true;
		GeneratorSwitcher (a_gens, false);
		GeneratorSwitcher (b_gens, false);
		GeneratorSwitcher (r_gens, true);
		GeneratorSwitcher (l_gens, false);
		GeneratorSwitcher (b_gens, false);
		r_destr.SetActive (false); 
		l_destr.SetActive (true); 
		a_destr.SetActive (true); 
		b_destr.SetActive (true);
		NotificationCenter.DefaultCenter ().PostNotification (this,"GoRight");

	}
	void GoLeft(){
		StopCoroutine ("moving");
		moving_down = false;
		moving_up = false;
		moving_left = true;
		moving_right = false;
		GeneratorSwitcher (b_gens, false);
		GeneratorSwitcher (r_gens, false);
		GeneratorSwitcher (l_gens, true);
		GeneratorSwitcher (b_gens, false);
		r_destr.SetActive (true); 
		l_destr.SetActive (false); 
		a_destr.SetActive (true); 
		b_destr.SetActive (true);
		NotificationCenter.DefaultCenter ().PostNotification (this,"GoLeft");


	}

	void GeneratorSwitcher(GameObject gens, bool switcher){

		foreach (Transform g in gens.transform) {
			if (switcher) {
				g.gameObject.SetActive (true);
				if (g.gameObject.GetComponent<stuff_creator> () != null) {
					g.gameObject.GetComponent<stuff_creator> ().SendMessage (gens.tag);
//					Debug.Log ("pinshadisimo");
				}
			} else {
				g.gameObject.SetActive (false);
			}
		}
	}
	public void OnDestroy(){
		StopAllCoroutines ();
	}

	}

