﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
public class UpdateGameOverScores : MonoBehaviour {
	public Text actual_score,hi_score,momo_score,plat_score,momo_record,plat_record;
	public Score le_score;
	UnityEvent unity_evnt;
	int gemi2;
	// Use this for initialization
	void Start () {
		unity_evnt = new UnityEvent ();
		unity_evnt.AddListener (gemi2turn);
		NotificationCenter.DefaultCenter ().AddObserver (this, "DordoIsdead");
	}

	void DordoIsdead(){
	
	}
	// Update is called once per frame
	void Update () {
	
	}
	void OnEnable(){
		gemi2 = StateGame.g_state.gemi2;
//		Debug.Log ("al empezar, los gemidos estan " + (gemi2 == 1 ? "activados" : "desactivados"));
		gemi2Switch (gemi2);
		actual_score.text = le_score.score.ToString("D5");
		momo_score.text = le_score.momo_count.ToString ("D3");
		plat_score.text = le_score.plat_count.ToString ("D3");
		momo_record.text = StateGame.g_state.momos.ToString ("D3");
		plat_record.text = StateGame.g_state.plats.ToString ("D3");
		hi_score.text = StateGame.g_state.hi_score.ToString ("D5");
			}

	void gemi2Switch(int gemi2_vl){
		switch (gemi2_vl) {
		case 0:
			GetComponent<AudioSource> ().mute = true;
			break;
		case 1:
			GetComponent<AudioSource> ().mute = false;
			break;
		default:
			Debug.Log ("el men llego por aqui...");
			GetComponent<AudioSource> ().mute = true;
			break;
		}
	}

	public void gemi2turn(){
		gemi2 = StateGame.g_state.gemi2;
		gemi2Switch (gemi2);
	}
}
