﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
public class LoadGameOver : MonoBehaviour {
	UnityEvent m_unityev;
	public string[] gemi2quotes = {"Apoko no krnal","Desactivar Gemi2"};
	public int dif = 0;
	public bool isgemi2quotes;
	void OnEnable(){
	}
		
	// Use this for initialization
	void Start () {
		m_unityev = new UnityEvent ();
		m_unityev.AddListener (UpdateGemi2);
		m_unityev.AddListener (SaveOptions);
		m_unityev.AddListener (ChangeAdvise);
		m_unityev.AddListener (UpdateDifficulty);
	}
	
	// Update is called once per frame
	void Update () {
		if (isgemi2quotes) {
			GetComponent<Text> ().text = Advise ();
		}
	}

	public void ChangeAdvise(){
		GetComponent<Text> ().text = Advise ();
	}
	public string Advise(){
		int i = GameObject.Find ("StateGame").GetComponent<StateGame> ().gemi2;
		switch (i) {
		case 0:
			return gemi2quotes [0];
		case 1 :
			return gemi2quotes [1];
		default:
			return "";

		}
	}
	public void UpdateGemi2(){
		GameObject.Find ("StateGame").SendMessage ("UpdateGemi2");
	}
	public void SaveOptions(){
		GameObject.Find ("StateGame").SendMessage ("SaveOptions");
	}
	public void UpdateDifficulty(){
		StateGame.g_state.difficulty = dif;
		Debug.Log (StateGame.g_state.difficulty);
	}
}
