﻿using UnityEngine;
using System.Collections;


public class SignBehavior : MonoBehaviour {
	
	public bool up, down, left, right;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D collider){
		chasercamera cam = GameObject.Find ("Camera").GetComponent<chasercamera>();
		if (collider.tag == "Player") {
			if (down && !cam.moving_down) {
				//NotificationCenter.DefaultCenter ().PostNotification (new Notification(this, "GoDown"));
	
				GameObject.Find ("Camera").SendMessage ("GoDown");

			}
			if (up && !cam.moving_up) {
				GameObject.Find ("Camera").SendMessage ("GoUp");

			}
			if (right && !cam.moving_right) {
				GameObject.Find ("Camera").SendMessage ("GoRight");


			}
			if (left && !cam.moving_left) {
				GameObject.Find ("Camera").SendMessage ("GoLeft");

			} else {
				NotificationCenter.DefaultCenter ().PostNotification (this, "ScoreIncrease", 5);
			}
			Destroy(transform.parent.gameObject);
		}
	}

}
