﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
/*
using System.Data;
using Mono.Data.Sqlite; 
*/
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class StateGame : MonoBehaviour {
	public string ambiencesound;
	public static StateGame g_state;
	string datapath,runner_datapath;
	public string runner_head,runner_body,runner_legs,runner_feet;
	public int hi_score,momos,plats;
	[Range(1,4)]
	public int difficulty = 1;
	[Range(0,1)]
	public int controlmode, gemi2 = 0,tutorial;
	public string runner_name = "Dordo";
	UnityEvent m_unityev;
	#if !UNITY_WEBGL
	//public IDbConnection dbconn;
	#endif

	 // Use this for initialization
	void Awake () {
		if (g_state == null) {
			g_state = this;
			DontDestroyOnLoad (gameObject);
		} else if (g_state != this) {
			Destroy (gameObject);

		}
		OnValidate ();

	}

	void OnValidate (){

		datapath = (Application.persistentDataPath + "/scores.dat");
		runner_datapath = (Application.persistentDataPath + "/runner.dat");
//		Debug.Log (Application.persistentDataPath);
//		dbconn = connection ();
		Load ();
	//LoadSerializedData ();
	}

	void Start () {
		

	}
	
	// Update is called once per frame
	void Update () {
		


	}
	/*
	IDbConnection connection(){
		string conn = "URI=file:" + Application.dataPath + "/jueguillo001.db"; //Path to database.
		IDbConnection dbconn = (IDbConnection) new SqliteConnection(conn);
		dbconn.Open(); //Open connection to the database.
		IDbCommand dbcmd = dbconn.CreateCommand();
	//string sqlQuery = "drop table if exists score";
		ArrayList querys = new ArrayList();
		querys.Add("CREATE TABLE IF NOT EXISTS score (id INTEGER,high_score INTEGER,momos INTEGER,plats INTEGER, UNIQUE(id))");
		querys.Add("INSERT OR IGNORE INTO score(id,high_score,momos,plats) VALUES (1,2,0,0)");
		querys.Add( "CREATE TABLE IF NOT EXISTS options (id INTEGER, gemi2 INTEGER,controlmode INTEGER,difficulty INTEGER, tutorial INTEGER,UNIQUE(id))");
		querys.Add( "INSERT OR IGNORE INTO options (id,gemi2,controlmode,difficulty,tutorial) VALUES (1,1,0,1,1)");
		querys.Add("CREATE TABLE IF NOT EXISTS runner (id INTEGER,head INTEGER,body INTEGER,legs INTEGER,feet INTEGER,name TEXT, UNIQUE(id))");
		querys.Add( "INSERT OR IGNORE INTO runner (id,head,body,legs,feet,name) VALUES (1,0,0,0,0,'"+name+"')");
		//dbcmd.CommandText = sqlQuery;
	//dbcmd.ExecuteNonQuery ();
		foreach(string q in querys){
			dbcmd.CommandText = q;
			dbcmd.ExecuteNonQuery ();
		}
		return dbconn;
	}
*/
	public void Load(){
		/*
			IDbCommand dbcmd = dbconn.CreateCommand ();
			string sqlQuery = "SELECT * FROM score";
			dbcmd.CommandText = sqlQuery;
	
			IDataReader reader = dbcmd.ExecuteReader ();

			while (reader.Read ()) {
				hi_score = reader.GetInt32 (1);
				momos = reader.GetInt32 (2);
				plats = reader.GetInt32 (3);
	
			}
			reader.Close ();
			//dbcmd.Dispose ();
	 
	 sqlQuery = "SELECT * FROM options";
			dbcmd.CommandText = sqlQuery;
			reader = dbcmd.ExecuteReader ();

				while (reader.Read ()) {
					gemi2 = reader.GetInt32 (1);
					controlmode = reader.GetInt32 (2);
					difficulty = reader.GetInt32 (3);
					tutorial = reader.GetInt32 (4);
					
			}
			reader.Close ();
			reader = null;
			dbcmd.Dispose ();
			dbcmd = null; 
		*/

		LoadPlayerPrefs ();
		LoadSerializedData ();


	}

	public void SaveScore(int new_hi_score,int new_momos,int new_plats){
		/*
			IDbCommand dbcmd = dbconn.CreateCommand ();
			string sqlQuery = "UPDATE score SET high_score = " + new_hi_score + ",momos = "+ new_momos +",plats = "+new_plats+" where id = 1";
			dbcmd.CommandText = sqlQuery;
			dbcmd.ExecuteNonQuery ();
			dbcmd.Dispose ();
			dbcmd = null;
*/
			SaveSerializedData ();

	}
	public void SaveAmbienceSound(string amsnd){
		ambiencesound = amsnd;
		PlayerPrefs.SetString ("ambience_sound", ambiencesound);


	}
	public void SaveOptions(/*IDbConnection dbconn,int cntrlmd,int gm2,int dffclty*/){
		/*
			IDbCommand dbcmd = dbconn.CreateCommand ();
			string sqlQuery = "UPDATE options SET gemi2 = " + gemi2 + ",controlmode= " + controlmode + ", difficulty = " + difficulty + " where id = 1";
			dbcmd.CommandText = sqlQuery;
			dbcmd.ExecuteNonQuery ();
			dbcmd.Dispose ();
			dbcmd = null;
*/
			PlayerPrefs.SetInt ("gemi2", gemi2);
			PlayerPrefs.SetInt ("controlmode", controlmode);
			PlayerPrefs.SetInt ("difficulty", difficulty);
			PlayerPrefs.SetInt ("tutorial", tutorial);
	
	}

	public void LoadPlayerPrefs(){
		if (PlayerPrefs.HasKey ("gemi2")) {
			gemi2 = PlayerPrefs.GetInt ("gemi2");
		} else {
			PlayerPrefs.SetInt ("gemi2",gemi2);
		}
		if (PlayerPrefs.HasKey ("controlmode")) {
			controlmode = PlayerPrefs.GetInt ("controlmode");
		} else {
			PlayerPrefs.SetInt ("controlmode", 0);
		}
		if (PlayerPrefs.HasKey ("difficulty")) {
			difficulty = PlayerPrefs.GetInt ("difficulty");
		} else {
			PlayerPrefs.SetInt ("difficulty", 1);
		}
		if (PlayerPrefs.HasKey ("tutorial")) {
			tutorial = PlayerPrefs.GetInt ("tutorial");
		} else {
			PlayerPrefs.SetInt ("tutorial", 1);
		}
		if (PlayerPrefs.HasKey ("ambience_sound")) {
			ambiencesound = PlayerPrefs.GetString ("ambience_sound");
		} else {
			PlayerPrefs.SetString ("ambience_sound", "pumpedupkids");
		}
//		Debug.Log ("pero los gemidos estan en "+gemi2+"!!!");

	}

	public void LoadSerializedData(){

		BinaryFormatter bf = new BinaryFormatter ();
		FileStream fs;
		if (File.Exists (datapath)) {
			fs	= File.Open (datapath, FileMode.Open);
			ScoreData scoredata = (ScoreData) bf.Deserialize (fs) ;
			hi_score = scoredata.highscore;
			momos = scoredata.momos;
			plats = scoredata.plats;
			fs.Close ();
		} else {
			hi_score = 2;
			momos = 2;
			plats = 2;
		}
		if (File.Exists (runner_datapath)) {
//			Debug.Log ("existe");
			fs	= File.Open (runner_datapath, FileMode.Open);
			RunnerData runner_data = bf.Deserialize (fs) as RunnerData;
			runner_head = runner_data.head;
			runner_body = runner_data.body;
			runner_legs = runner_data.legs;
			runner_feet = runner_data.feet;
			fs.Close ();
		} else {
//			Debug.Log (" no existe");
			runner_head = "grasoso";
			runner_body = "grasoso";
			runner_legs = "grasoso";
			runner_feet = "grasoso";
		}
	}
	public void SaveSerializedData(){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream fs = File.Create (datapath);
		ScoreData scoredata= new ScoreData (hi_score, momos, plats);
		bf.Serialize (fs, scoredata);
		fs.Close ();

}
	public void SaveRunnerData(){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream fs = File.Create (runner_datapath);
		RunnerData rnrdta = new RunnerData (runner_head, runner_body, runner_legs, runner_feet, runner_name);

		bf.Serialize (fs, rnrdta);
		fs.Close ();
	}


[Serializable]
class ScoreData{
	public int highscore,momos,plats;
	public ScoreData(int hi_s,int m,int p){
		highscore = hi_s;
		momos = m;
		plats = p;
	}
}

	[Serializable]
	class RunnerData{
		
		public string name,head,body,legs,feet;
		public RunnerData(string h,string bd,string bt,string f,string n){
			head = h;
			body = bd;
			legs = bt;
			feet = f;
			name = n;
		}
	}

}