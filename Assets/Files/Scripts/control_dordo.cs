﻿ using UnityEngine;
using System.Collections;

public class control_dordo : MonoBehaviour {
	public float jumpforce = 100f;
	public float velocity = 1f;
	public bool onground = true;
	public Transform le_trns;
	public float check_radius = 0.07f;
	public LayerMask platforms;
	public int jumpcounter = 2;
	int jumps = 0;

	bool goright = false;
	Animator anim;
	Rigidbody2D rb2d;

	public Transform trns ;
	// Use this for initialization
	void Start () {
		NotificationCenter.DefaultCenter ().AddObserver (this, "ArgieGoesRight");
		NotificationCenter.DefaultCenter ().AddObserver (this, "ArgieGoesLeft");
		NotificationCenter.DefaultCenter ().AddObserver (this, "ArgieJumps");
		NotificationCenter.DefaultCenter ().AddObserver (this, "ArgieStops");
	}
	void Awake(){
		anim = GetComponent<Animator> ();
		rb2d = GetComponent<Rigidbody2D> ();
		trns = GetComponent<Transform> ();
	}

	void FixedUpdate(){
		if (onground) {
			jumps = jumpcounter;
		}

		onground = Physics2D.OverlapCircle (le_trns.position, check_radius,platforms);
		anim.SetBool ("landed", onground);

	}
	// Update is called once per frame
	void Update ()
	{

		//TestingDirections ();
		/*Vector3 direction = trns.transform.localScale;
		if (Input.GetKey (KeyCode.D)) {
			
			rb2d.velocity = new Vector2 (velocity, rb2d.velocity.y);
			goright = true;
			if (trns.localScale.x < 0) {
				direction.x *= -1;
				trns.localScale = direction;
			}
		}

		if ( Input.GetKey (KeyCode.A)) {
			if (trns.localScale.x > 0) {
				direction.x *= -1;
				trns.localScale = direction;
								}
			rb2d.velocity = new Vector2 (-1*velocity, rb2d.velocity.y);
			goright = true;
		}

		if(Input.GetKeyUp(KeyCode.D))
			{
			rb2d.velocity = new Vector2 (0, rb2d.velocity.y);
			goright = false;}
		if(Input.GetKeyUp(KeyCode.A))
		{
			rb2d.velocity = new Vector2 (0, rb2d.velocity.y);
			goright = false;}

		if (Input.GetKeyDown(KeyCode.Space)) {
			if (canjump || onground) {
				//rb2d.velocity.y = 0;   
				//rb2d.AddForce (new Vector2 (0, jumpforce));
				rb2d.velocity = new Vector2 (rb2d.velocity.x,jumpforce);
				doublejump = true;
				GetComponent<AudioSource> ().Play();
			} 
	
			if (doublejump||canjump) {
//					rb2d.velocity.y = 0;
					//rb2d.AddForce (new Vector2 (0, jumpforce));
				rb2d.velocity = new Vector2 (rb2d.velocity.x,jumpforce);
				doublejump = false;
					canjump = false;
				GetComponent<AudioSource> ().Play();

				}
			     
		
			if (!doublejump) {

				doublejump = true;

		}
			
		}
*/
		anim.SetBool ("running_right", goright);
	}
	
	void TestingDirections(){
		
		if (Event.current.Equals(Event.KeyboardEvent("down"))) {
			//NotificationCenter.DefaultCenter ().PostNotification (new Notification(this, "GoDown"));
			Debug.Log("repito");
			GameObject.FindWithTag("MainCamera").SendMessage ("GoDown");

		}
		if (Event.current.Equals(Event.KeyboardEvent("up"))) {
			GameObject.Find ("Camera").SendMessage ("GoUp");

		}
		if (Event.current.Equals(Event.KeyboardEvent("right"))) {
			GameObject.Find ("Camera").SendMessage ("GoRight");
//			Debug.Log ("pinchar el boton genero estos...");

		}
		if (Event.current.Equals(Event.KeyboardEvent("left"))) {
			GameObject.Find ("Camera").SendMessage ("GoLeft");

		}
	}


		void OnGUI(){
		//TestingDirections ();
	}



	

	void ArgieGoesRight(Notification notification){
		
		goright = true;
		//while (goright) {
			Vector3 direction = trns.transform.localScale;
			rb2d.velocity = new Vector2 (velocity, rb2d.velocity.y);
			if (trns.localScale.x < 0) {
				direction.x *= -1;
				trns.localScale = direction;
			}
		//}

	}

	void ArgieGoesLeft (Notification notification)
	{
		goright = true;
		//while (goright) {
			Vector3 direction = trns.transform.localScale;
			rb2d.velocity = new Vector2 (velocity, rb2d.velocity.y);

			if (trns.localScale.x > 0) {
				direction.x *= -1;
				trns.localScale = direction;
			}
			rb2d.velocity = new Vector2 (-1 * velocity, rb2d.velocity.y);
		//}

			
		
	}

	public void ArgieJumps(Notification notification){
		jumps--;
		if (jumps > 0) {
			//rb2d.AddForce (new Vector2 (0, jumpforce));
			rb2d.velocity = new Vector2 (rb2d.velocity.x,jumpforce);
			GetComponent<AudioSource> ().Play();

		};
	}
	void ArgieStops(Notification notification){
		rb2d.velocity = new Vector2 (0, rb2d.velocity.y);
		goright = false;
	}
}
