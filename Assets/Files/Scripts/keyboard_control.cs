﻿using UnityEngine;
using System.Collections;

public class keyboard_control : MonoBehaviour {
	public KeyCode leftb,rightb,jumpb;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(jumpb)) {
			NotificationCenter.DefaultCenter ().PostNotification (this,"ArgieJumps");
		}
		if (Input.GetKey (rightb)) {
			NotificationCenter.DefaultCenter ().PostNotification (this,"ArgieGoesRight");
		}
		if (Input.GetKey (leftb)) {
			NotificationCenter.DefaultCenter ().PostNotification (this,"ArgieGoesLeft");
		}

		if (Input.GetKeyUp (rightb) || Input.GetKeyUp (leftb)) {
			NotificationCenter.DefaultCenter ().PostNotification (this,"ArgieStops");
		}
	}
}
