﻿using UnityEngine;
using System.Collections;

public class ComboCenter : MonoBehaviour {
	int plat_combocounter = 0;
	int momo_combocounter = 0;
	string plat_tag;
	public int momo_combo_starter = 5;
	public int plat_combo_starter = 5;
	public int combo_points = 1;
	bool plat_comboeffect = false;
	bool momo_comboeffect = false;
	public Animator plat_combo_anim, momo_combo_amim;
	// Use this for initialization
	void Awake(){
		plat_tag = "";
	}
	void Start () {
	NotificationCenter.DefaultCenter ().AddObserver (this, "PlatformSteped");
	NotificationCenter.DefaultCenter ().AddObserver (this, "MomoComboIsDead");
	NotificationCenter.DefaultCenter ().AddObserver (this, "MomoCombo");
	plat_comboeffect = false;
 momo_comboeffect = false;
	}

	void PlatformSteped(Notification notification){
		//Debug.Log ("me llamaron");
		GameObject actlgmbjct = (GameObject)notification.data;
		string actltg = actlgmbjct.tag;
	
		if (actltg == plat_tag) {
			plat_combocounter++;
//			Debug.Log (plat_combocounter + " salto(s)");
		} else {
//			Debug.Log ("no prro, la kgast");
			plat_combocounter = 0;
			plat_tag = actltg;
			plat_comboeffect = false;

		}
		ComboMaker (plat_combocounter, plat_combo_starter, combo_points,"plataformas",ref plat_comboeffect);
	}

	void MomoCombo(Notification notification){
		momo_combocounter++;
//		Debug.Log (momo_combocounter + " momo(s) capturado(s)");
		ComboMaker (momo_combocounter, momo_combo_starter, combo_points, "momos",ref momo_comboeffect);
	}

	void MomoComboIsDead(Notification notification){
		momo_combocounter = 0;
		momo_comboeffect = false;
//		Debug.Log ("mira men pero que mal combo de momos :'v");


	}
	void ComboMaker(int counter, int starter,int points,string obj, ref bool combo_effect){
		if (counter >= starter) {
			NotificationCenter.DefaultCenter ().PostNotification (this, "ScoreIncrease", points);
			combo_effect = true;
			if (counter % starter == 0) {
				Debug.Log ("¡¡combo de " + obj + "!!");

				combo_points++;
			}
		} else {
			combo_effect = false;}
	}

	// Update is called once per frame
	void Update () {
		//Debug.Log ("plat_combo: " + plat_comboeffect);
		//Debug.Log ("momo_combo: " + momo_comboeffect);
		if (plat_comboeffect) {
			NotificationCenter.DefaultCenter ().PostNotification (this, "PlatComboStarted", plat_tag);
			plat_combo_anim.SetBool ("has_combo", true);
		
		} else {
			NotificationCenter.DefaultCenter ().PostNotification (this, "PlatComboRuined");
			plat_combo_anim.SetBool ("has_combo", false);
			plat_combo_anim.SetInteger ("ruined_style", 2);
		}
		if (momo_comboeffect) {
			momo_combo_amim.SetBool ("has_combo", true);
		}
		if(!momo_comboeffect) {
			momo_combo_amim.SetBool ("has_combo", false);
			momo_combo_amim.SetInteger ("ruined_style", 2);

		}
	}
}
