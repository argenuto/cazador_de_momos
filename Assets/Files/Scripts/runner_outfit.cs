﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class runner_outfit : MonoBehaviour {

	// Use this for initialization
	void Start () {
		LoadOutfitFromMenu ();
	}
	void OnEnable(){
		
	}
	// Update is called once per frame
	void Update () {
		
	}

	void LoadOutfitFromMenu(){
		string body_path="characters/"+StateGame.g_state.runner_body, 
		head_path ="characters/"+StateGame.g_state.runner_head,
		feet_path ="characters/"+StateGame.g_state.runner_feet,
		legs_path="characters/"+StateGame.g_state.runner_legs;
		GameObject.Find ("Dordo/tronco").GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> (body_path+"/body");
		GameObject.Find ("Dordo/antebrazofrontal").GetComponent<SpriteRenderer> ().sprite =Resources.Load<Sprite> (body_path+"/forearm");
		GameObject.Find ("Dordo/antebrasotrasero").GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> (body_path+"/forearm");
		GameObject.Find ("Dordo/antebrazofrontal/brazofrontal").GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> (body_path+"/arm");
		GameObject.Find ("Dordo/antebrasotrasero/brazotrasero").GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> (body_path+"/arm");
		GameObject.Find ("Dordo/gluteofrontal").GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> (legs_path+"/frontgluts");
		GameObject.Find ("Dordo/gluteoposterior").GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> (legs_path+"/backgluts");
		GameObject.Find ("Dordo/gluteofrontal/canillafrontal").GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> (legs_path+"/tibia2");
		GameObject.Find ("Dordo/gluteoposterior/canillatrasera").GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> (legs_path+"/tibia1");
		GameObject.Find ("Dordo/cabeza").GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> (head_path+"/head");
		GameObject.Find ("Dordo/gluteofrontal/canillafrontal/zapatofrontal").GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> (feet_path+"/shoe");
		GameObject.Find ("Dordo/gluteoposterior/canillatrasera/zapatotrasero").GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> (feet_path+"/shoe");
	}
}
