﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BodyPartsManager : MonoBehaviour {
	public enum bodyparts{head,chest_arms,legs,feet};
	public bodyparts bdprts;
	public string[] body_sets;
	public Sprite[] bdy_sprts;
	public GameObject text_prompter;
	ArrayList sprite_groups;
	List<chest_arms> ca_list;
	List<head> h_list;
	List<feet> f_list;
	List<legs> l_list;
	int pointer = 0;
	// Use this for initialization
	void OnValidate(){
		switch (bdprts) {
		case bodyparts.chest_arms:
			ca_list = new List<chest_arms> ();
			break;
		case bodyparts.head:
			h_list = new List<head> ();

			break;
		case bodyparts.feet:
			f_list = new List<feet> ();
			break;
		case bodyparts.legs:
			l_list = new List<legs> ();
			break	;
		default:
			break	;
		}
		foreach (string bp in body_sets) {
			bdy_sprts = Resources.LoadAll<Sprite> ("characters/" + bp) ;
//			Debug.Log ("characters/" + bp);
			switch (bdprts) {
			case bodyparts.chest_arms:
				
				Sprite fa = FindSprite (bdy_sprts, "forearm");
				Sprite a = FindSprite (bdy_sprts, "arm");
				Sprite b = FindSprite (bdy_sprts, "body");
				chest_arms c = new chest_arms (fa, a, b, bp);
				ca_list.Add (c);

				break;
			case bodyparts.head:
			//	Debug.Log ("ia yegue:v");
				Sprite hd = FindSprite (bdy_sprts, "head");
				head h = new head (hd,bp);
				h_list.Add (h);
			//	Debug.Log ("cabezas: " + h_list.Count);

				break;
			case bodyparts.feet:
				Sprite f = FindSprite (bdy_sprts, "shoe");
				feet ft = new feet (f,bp);
				f_list.Add (ft);

				break;
			case bodyparts.legs:
				Sprite fg = FindSprite (bdy_sprts, "frontgluts");
				Sprite bg = FindSprite (bdy_sprts, "backgluts");
				Sprite t1 = FindSprite (bdy_sprts, "tibia1");
				Sprite t2 = FindSprite (bdy_sprts, "tibia2");
				legs lgs = new legs (fg, bg, t1, t2,bp);
				l_list.Add (lgs);

				break	;
			default:
				break	;
			}


		}
	}
	void Awake(){
		OnValidate ();

	}
	void Start () {
		pointer = ActualPointer ();
		UnityEvent my_uevent;
		my_uevent = new UnityEvent ();
		my_uevent.AddListener (GoNext);
		my_uevent.AddListener(GoPrevious);
		switch (bdprts) {
		case bodyparts.chest_arms:
			text_prompter.GetComponent<Text> ().text = StateGame.g_state.runner_body;
			break;
		case bodyparts.head:
			text_prompter.GetComponent<Text> ().text = StateGame.g_state.runner_head;
			break;
		case bodyparts.feet:
			text_prompter.GetComponent<Text> ().text = StateGame.g_state.runner_feet;
			break;
		case bodyparts.legs:
			text_prompter.GetComponent<Text> ().text = StateGame.g_state.runner_legs;
			break	;
		default:
			break	;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void GoNext(){
		pointer++;
		Debug.Log (bdprts.ToString() + ": " + pointer);
		Move (pointer);
	}
	public void GoPrevious(){
		if (pointer == 1) {
			pointer--;
		}
		pointer--;
		Debug.Log (bdprts.ToString() + ": " + pointer);
		Move (pointer);
	}

	void Move(int pointer){
		switch (bdprts) {
		case bodyparts.chest_arms: 
			chest_arms chesta = ca_list [(int)Mathf.Abs (pointer % ca_list.Count)];
			chesta.PutImage();
			text_prompter.GetComponent<Text> ().text = chesta.body_set;
			break;
		case bodyparts.feet:
			feet le_feet = f_list [(int) Mathf.Abs (pointer % f_list.Count)];
			le_feet.PutImage ();
			text_prompter.GetComponent<Text> ().text = le_feet.body_set;
			break;
		case bodyparts.head:
			head le_head = h_list [(int) Mathf.Abs (pointer % h_list.Count)];
			le_head.PutImage ();
			text_prompter.GetComponent<Text> ().text = le_head.body_set;
			break;
		case bodyparts.legs:
			legs le_legs = l_list [(int)Mathf.Abs (pointer % l_list.Count)];
			le_legs.PutImage ();
			text_prompter.GetComponent<Text> ().text = le_legs.body_set;
			break;
		default:
			break;
		}
	}



	Sprite FindSprite(Sprite[] spritearray,string s_name){
		Sprite found = new Sprite();
		foreach (Sprite s in spritearray) {
//			Debug.Log (s.name);
			if (s.name == s_name) {
				found = s;
			}
		}
		return found;
	}

	int ActualPointer(){
		int pointer = 0;
		for (int i = 0; i < body_sets.Length; i++) {

		switch (bdprts) {
			case bodyparts.chest_arms: 
				if (StateGame.g_state.runner_body == body_sets [i]) {
					pointer = i;
				}	
			break;
		case bodyparts.feet:
				if (StateGame.g_state.runner_feet == body_sets [i]) {
					pointer = i;
				}	
			break;
		case bodyparts.head:
				if (StateGame.g_state.runner_head == body_sets [i]) {
					pointer = i;
				}	
			break;
		case bodyparts.legs:
				if (StateGame.g_state.runner_legs == body_sets [i]) {
					pointer = i;
				}	
			break;
		default:
			break;
			}
		}
		return pointer;
}

struct chest_arms{
	Sprite forearm,arm,body;
	public string body_set;
	public chest_arms (Sprite fa, Sprite a, Sprite b,string bs){
		body_set = bs;
		forearm = fa;
		arm = a;
		body = b;

	}
	public void PutImage(){
		GameObject.Find ("Dordo/tronco").GetComponent<SpriteRenderer>().sprite = body;
		GameObject.Find ("Dordo/antebrazofrontal").GetComponent<SpriteRenderer> ().sprite = forearm;
		GameObject.Find ("Dordo/antebrasotrasero").GetComponent<SpriteRenderer> ().sprite = forearm;
		GameObject.Find ("Dordo/antebrazofrontal/brazofrontal").GetComponent<SpriteRenderer> ().sprite = arm;
		GameObject.Find ("Dordo/antebrasotrasero/brazotrasero").GetComponent<SpriteRenderer> ().sprite = arm;
	}

}

struct legs{
	Sprite front_gluts,back_gluts,tibia1,tibia2;
	public string body_set;
	public legs(Sprite fg,Sprite bg,Sprite t1, Sprite t2,string bs){
		body_set = bs;
		front_gluts = fg;
		back_gluts = bg;
		tibia1 = t1;
		tibia2 = t2;
	}
	public void PutImage(){
		GameObject.Find ("Dordo/gluteofrontal").GetComponent<SpriteRenderer> ().sprite = front_gluts;
		GameObject.Find ("Dordo/gluteoposterior").GetComponent<SpriteRenderer> ().sprite = back_gluts;
		GameObject.Find ("Dordo/gluteofrontal/canillafrontal").GetComponent<SpriteRenderer> ().sprite = tibia2;
		GameObject.Find ("Dordo/gluteoposterior/canillatrasera").GetComponent<SpriteRenderer> ().sprite = tibia1;


	}

}

struct head{
	Sprite _head;
	public string body_set;
	public head(Sprite _h,string bs){
		body_set = bs;
		_head = _h;
	}
	public void PutImage(){
		GameObject.Find ("Dordo/cabeza").GetComponent<SpriteRenderer> ().sprite = _head;
	}
}
struct feet{
	Sprite foot;
	public string body_set;
	public feet(Sprite f,string bs){
		body_set = bs;
		foot = f;
	}
	public void PutImage(){
		GameObject.Find ("Dordo/gluteofrontal/canillafrontal/zapatofrontal").GetComponent<SpriteRenderer> ().sprite = foot;
		GameObject.Find ("Dordo/gluteoposterior/canillatrasera/zapatotrasero").GetComponent<SpriteRenderer> ().sprite = foot;

	}
}

}
