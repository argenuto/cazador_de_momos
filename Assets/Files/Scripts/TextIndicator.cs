﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextIndicator : MonoBehaviour {
	public string[] up_text,down_text,right_text,left_text;
	public float transition_speed = 1f;
	Text txt;

	// Use this for initialization
	void Start () {
		NotificationCenter.DefaultCenter ().AddObserver (this, "GoDown");
		NotificationCenter.DefaultCenter ().AddObserver (this, "GoUp");
		NotificationCenter.DefaultCenter ().AddObserver (this, "GoRight");
		NotificationCenter.DefaultCenter ().AddObserver (this, "GoLeft");
		txt = GetComponent<Text> ();
		txt.text = "";
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator TextAnimations(string[] textlist){
		while (true) {
			foreach (string t in textlist) {
				txt.text = t;
				yield return new WaitForSeconds (1f*transition_speed);
			}
		}

	
	}
	void GoUp(Notification notification){
		StopAllCoroutines ();
		StartCoroutine (TextAnimations(up_text));
	}

	void GoDown(Notification notification){
		StopAllCoroutines ();
		StartCoroutine (TextAnimations(down_text));
	}

	void GoRight(Notification notification){
		StopAllCoroutines ();
		Debug.Log ("pare y empece otra vez");
		StartCoroutine (TextAnimations(right_text));
	}

	void GoLeft(Notification notification){
		StopAllCoroutines ();
		StartCoroutine (TextAnimations(left_text));
	}
}
