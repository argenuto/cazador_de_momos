﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;
public struct transdata{
	public 	GameObject goactive,goinactive;
	public int trans_num;
	public float trans_time;
	public transdata(GameObject goinact,GameObject gact,int t_num,float t_tm){
		goinactive = goinact;
		goactive = gact;
		trans_num = t_num;
		trans_time = t_tm;
	}
}


public class ButtonActions : MonoBehaviour {
	public GameObject main_menu,option_menu,custom_menu,creditcamera,dordo;
	public GameObject[] momocreators;
	UnityEvent m_MyEvent;

	void Invoke(){
		main_menu.SetActive (true);
		option_menu.SetActive (false);
		custom_menu.SetActive (false);
		creditcamera.SetActive (false);
	}
	void Start() {
		m_MyEvent = new UnityEvent ();
		m_MyEvent.AddListener (ShowOptionDataInScreen);
		m_MyEvent.AddListener (EnterMainMenu);
		m_MyEvent.AddListener (EnterOptionMenu);
		m_MyEvent.AddListener (EnterCustomMenu);
		m_MyEvent.AddListener (EnterCredits);
		m_MyEvent.AddListener (UpdateOptionData);
		m_MyEvent.AddListener (UpdateGemi2);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void UpdateOptionData(){
		StateGame.g_state.gemi2 = (int) GameObject.Find ("moaning_slider").GetComponent<Slider> ().value;
		//		controlmode = (int) GameObject.Find ("controlmode_slider").GetComponent<Slider> ().value;
		//		tutorial = (int) GameObject.Find ("tutorial_slider").GetComponent<Slider> ().value;
		GameObject df = GameObject.Find ("difficulty");
		int i = 1;
		foreach(Transform df_sl in df.transform){
			if (df_sl.gameObject.GetComponent<Toggle>().isOn) {
				StateGame.g_state.difficulty = i;
			}
			i++;
		}
		StateGame.g_state.SaveOptions ();
	}

	public void UpdateCustomData(){

	StateGame.g_state.runner_head = GameObject.Find ("head_selection").GetComponent<Text> ().text;
		StateGame.g_state.runner_body = GameObject.Find ("body_selection").GetComponent<Text> ().text;
		StateGame.g_state.runner_legs = GameObject.Find ("legs_selection").GetComponent<Text> ().text;
		StateGame.g_state.runner_feet = GameObject.Find ("feet_selection").GetComponent<Text> ().text;
		AudioManager.aumanger.PlayMainThemeByChara ("marcianito", "marcianito");
		StateGame.g_state.SaveRunnerData ();
	}



	public void UpdateGemi2(){
		StateGame.g_state.gemi2 = (StateGame.g_state.gemi2 == 1 ? 0:1);
		Debug.Log("los gemidos estan "+(StateGame.g_state.gemi2 == 1 ? "activados" : "desactivados"));
	}
	public	void EnterMainMenu(){
		GameObject[] guiopts = {option_menu, custom_menu, creditcamera };
		GameObject actl_gui = new GameObject();
		foreach (GameObject go in guiopts) {
			if (go.activeInHierarchy == true) {
				actl_gui = go;
			}
		}
		transdata opt_td = new transdata (actl_gui,main_menu, -1, 0.5f);
		StartCoroutine(Transition(opt_td));
	}

	public void EnterOptionMenu(){
		transdata opt_td = new transdata (main_menu,option_menu, 3, 2f);
		StartCoroutine(Transition(opt_td));

	}
	public void EnterCustomMenu(){

		StartCoroutine ("CustomEnter");


	}

	public void EnterCredits(){
		transdata crdt_dt = new transdata (main_menu,creditcamera, 1, 2f);
		StartCoroutine(Transition(crdt_dt));
	}

	IEnumerator Transition(transdata td){
		Animator animcustom = GameObject.Find ("Main_Canvas").gameObject.GetComponent<Animator> ();
		animcustom.SetInteger ("transition", td.trans_num);
		foreach (GameObject go in momocreators) {
			go.SetActive ( !go.activeInHierarchy);
		}
		yield return new WaitForSeconds (td.trans_time);
		td.goactive.SetActive (true);
		td.goinactive.SetActive (false);
		yield return null;
	}

	IEnumerator CustomEnter(){
		transdata crdt_dt = new transdata (main_menu,custom_menu, 2, 1.5f);
		yield return StartCoroutine(Transition(crdt_dt));
		dordo.SetActive (true);
	}
	public void ShowOptionDataInScreen(){
		foreach (Transform t in option_menu.transform) {
			if(t.gameObject.name == "moaning_slider"){
				t.gameObject.GetComponent<Slider> ().value = StateGame.g_state.gemi2;
			}
			if(t.gameObject.name == "controlmode_slider"){
				t.gameObject.GetComponent<Slider> ().value = StateGame.g_state.controlmode;
			}
			if(t.gameObject.name == "tutorial_slider"){
				t.gameObject.GetComponent<Slider> ().value = StateGame.g_state.tutorial;
			}
			if (t.gameObject.name == "difficulty") {
				int i = 1;
				foreach(Transform df_sl in t){
					if (StateGame.g_state.difficulty == i) {
						df_sl.gameObject.GetComponent<Toggle> ().isOn = true;
					}
					i++;
				}
			}
		}
			

	}
}
